(function ($) {
  'use strict';

  // Magic number: DO NOT CHANGE
  var DICE_SIZE = 256;
  // Step size for pan controls: FEEL FREE TO CHANGE
  var PAN_STEP = 128;
  var BLANK = 'data:image/png;base64,' +
      'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAA' +
      'AAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII=';

  // Internal utility functions
  function getOffset(topLeftPx) {
    return [Math.floor(topLeftPx[0] / DICE_SIZE), Math.floor(topLeftPx[1] / DICE_SIZE)];
  }

  function getImagePixelFromEvent($viewport, topLeftPx, event) {
    var viewportPageOffset = $viewport.offset();
    return [
      event.pageY - viewportPageOffset.top + topLeftPx[0],
      event.pageX - viewportPageOffset.left + topLeftPx[1]
    ];
  }

  function fillBlock(elem, coords, offset, zoomLevel, imgData) {
    var y = (coords[0] + offset[0]) * DICE_SIZE;
    var x = (coords[1] + offset[1]) * DICE_SIZE;
    var imgWidth = imgData.sizes[zoomLevel].width;
    var imgHeight = imgData.sizes[zoomLevel].height;
    if (y >= 0 && y < imgHeight && x >= 0 && x < imgWidth) {
      var url = imgData.url + imgData.path + '/' + imgWidth + '_' + imgHeight + '/';
      elem.style.backgroundImage = 'url(' + url + y + '_' + x + ')';
    } else {
      elem.style.backgroundImage = 'none';
    }
  }

  function fillViewport($wrapper, offset, zoomLevel, imgData) {
    var rows = $wrapper.children();
    var i, j, elem;
    for (i = 0; i < rows.length; ++i) {
      for (j = 0; j < rows[i].childElementCount; ++j) {
        elem = rows[i].childNodes[j];
        fillBlock(elem, [i, j], offset, zoomLevel, imgData);
      }
    }
  }

  function initViewport($viewport) {
    var height = Math.ceil($viewport.height() / DICE_SIZE) + 1;
    var width = Math.ceil($viewport.width() / DICE_SIZE) + 1;
    var viewportElem = $viewport.get()[0];
    var wrapper = document.createElement('div');
    wrapper.className = 'diced-image-wrapper';
    wrapper.style.width = width * DICE_SIZE + 'px';
    wrapper.style.height = height * DICE_SIZE + 'px';
    var imgRow = document.createElement('div');
    imgRow.className = 'diced-image-row';
    var imgDiv = document.createElement('div');
    var blank = document.createElement('img');
    blank.src = BLANK;
    imgDiv.appendChild(blank);
    imgDiv.className = 'diced-image';

    var i, j, newRow;
    for (i = 0; i < height; ++i) {
      newRow = imgRow.cloneNode(true);
      for (j = 0; j < width; ++j) {
        newRow.appendChild(imgDiv.cloneNode(true));
      }
      wrapper.appendChild(newRow);
    }
    viewportElem.appendChild(wrapper);
    return $(wrapper);
  }

  function makeBigPicture(imgData) {

    var $container = this;

    var zoomLevel = 0, topLeftPx;
    while (imgData.sizes[zoomLevel].width < $container.width()) { zoomLevel++; }

    var zoom = parseInt($container.attr('data-zoom'), 10);
    if (zoom < imgData.sizes.length && zoom >= 0) {
      zoomLevel = zoom;
    }
    var pos = $container.attr('data-position');
    if (
      pos === undefined ||
        pos.split('x').length !== 2 ||
        isNaN(pos.split('x')[0]) ||
        isNaN(pos.split('x')[1])
    ) { topLeftPx = [0, 0]; } else {
      topLeftPx = pos.split('x').map(Number);
    }

    var $viewport = $('<div class="viewport"></div>').appendTo($container);
    var $wrapper = initViewport($viewport);

    $.extend($wrapper, {
      jumpRight: function () {
        var rows = this.children();
        var leftCol = rows.children(':first-child').detach();
        var lastIndex = rows[0].childElementCount - 1;
        leftCol.each(function (index, elem) {
          $(elem).insertAfter(rows[index].childNodes[lastIndex]);
          fillBlock(elem, [index, lastIndex + 1], getOffset(topLeftPx), zoomLevel, imgData);
        });
      },
      jumpLeft: function () {
        var rows = this.children();
        var rightCol = rows.children(':last-child').detach();
        rightCol.each(function (index, elem) {
          $(elem).insertBefore(rows[index].childNodes[0]);
          fillBlock(elem, [index, 0], getOffset(topLeftPx), zoomLevel, imgData);
        });
      },
      jumpDown: function () {
        var lastIndex = this.children().length - 1;
        var topRow = this.children(':first-child').detach();
        topRow.children().each(function (index, elem) {
          fillBlock(elem, [lastIndex, index], getOffset(topLeftPx), zoomLevel, imgData);
        });
        this.children(':last-child').after(topRow);
      },
      jumpUp: function () {
        var bottomRow = this.children(':last-child').detach();
        bottomRow.children().each(function (index, elem) {
          fillBlock(elem, [0, index], getOffset(topLeftPx), zoomLevel, imgData);
        });
      }
    });

    fillViewport($wrapper, getOffset(topLeftPx), zoomLevel, imgData);

    // Available variables:
    // imgData, zoomLevel, topLeftPx, $container, $viewport, $wrapper
    var bigPicture = {
      zoomOut: function (centerPx) {
        if (zoomLevel > 0) {
          zoomLevel--;
          var newCenterPx = [Math.floor(centerPx[0] / 2), Math.floor(centerPx[1] / 2)];
          topLeftPx = [
            Math.max(0, newCenterPx[0] - Math.floor($viewport.height() / 2)),
            Math.max(0, newCenterPx[1] - Math.floor($viewport.width() / 2))
          ];
          fillViewport($wrapper, getOffset(topLeftPx), zoomLevel, imgData);
        }
      },
      zoomIn: function (centerPx) {
        if (zoomLevel < imgData.sizes.length - 1) {
          zoomLevel++;
          var newCenterPx = [centerPx[0] * 2, centerPx[1] * 2];
          var maxLeft = imgData.sizes[zoomLevel].width - $viewport.width();
          var maxTop = imgData.sizes[zoomLevel].height - $viewport.height();
          topLeftPx = [
            Math.min(maxTop, newCenterPx[0] - Math.floor($viewport.height() / 2)),
            Math.min(maxLeft, newCenterPx[1] - Math.floor($viewport.width() / 2))
          ];
          fillViewport($wrapper, getOffset(topLeftPx), zoomLevel, imgData);
        }
      },
      pan: function (dir, step) {
        var y = topLeftPx[0];
        var x = topLeftPx[1];
        var maxLeft = imgData.sizes[zoomLevel].width - $viewport.width();
        var maxTop = imgData.sizes[zoomLevel].height - $viewport.height();
        var newMarginTop, newMarginLeft;
        switch (dir) {
        case 'up':
          step = -step; // Fall through intentional
        case 'down':
          // if (
          //   (y + step >= 0 || step < 0) && // new y is > 0 or direction is down
          //     (y + step < maxTop || step > 0) // new y is < max or direction is up
          // ) {
          topLeftPx = [y + step, x];
          newMarginTop = parseInt($wrapper.css('margin-top'), 10) - step;
          if (newMarginTop < -DICE_SIZE) {
            newMarginTop += DICE_SIZE;
            $wrapper.jumpDown();
          } else if (newMarginTop > 0) {
            newMarginTop -= DICE_SIZE;
            $wrapper.jumpUp();
          }
          $wrapper.css('margin-top', newMarginTop);
          // }
          break;
        case 'left':
          step = -step; // Fall through intentional
        case 'right':
          // if (
          //   (x + step >= 0 || step > 0) &&
          //     (x + step < maxLeft || step < 0)
          // ) {
          topLeftPx = [y, x + step];
          newMarginLeft = parseInt($wrapper.css('margin-left'), 10) - step;
          if (newMarginLeft < -DICE_SIZE) {
            newMarginLeft += DICE_SIZE;
            $wrapper.jumpRight();
          } else if (newMarginLeft > 0) {
            newMarginLeft -= DICE_SIZE;
            $wrapper.jumpLeft();
          }
          $wrapper.css('margin-left', newMarginLeft);
          // }
          break;
        default:
          console.error('Unrecognised argument to pan function');
        }
      },
      moveTo: function (top, left) {
        topLeftPx = [top, left];
        fillViewport($wrapper, getOffset(topLeftPx), zoomLevel, imgData);
      }
    };

    if (!$container.hasClass('no-controls')) {
      $('<i class="zoom-in fa fa-search-plus"></i>')
        .appendTo($container)
        .click(function () {
          var centerPx = [
            topLeftPx[0] + $viewport.height() / 2,
            topLeftPx[1] + $viewport.width() / 2
          ];
          bigPicture.zoomIn(centerPx);
        });
      $('<i class="zoom-out fa fa-search-minus"></i>')
        .appendTo($container)
        .click(function () {
          var centerPx = [
            topLeftPx[0] + $viewport.height() / 2,
            topLeftPx[1] + $viewport.width() / 2
          ];
          bigPicture.zoomOut(centerPx);
        });
      var controls = $('<div class="controls"></div>').appendTo($container);

      $('<img class="pan-up"><img class="pan-down">' +
        '<img class="pan-left"><img class="pan-right">')
        .attr('src', BLANK)
        .appendTo(controls);
      controls.find('.pan-up').click(function () { bigPicture.pan('up', PAN_STEP); });
      controls.find('.pan-down').click(function () { bigPicture.pan('down', PAN_STEP); });
      controls.find('.pan-left').click(function () { bigPicture.pan('left', PAN_STEP); });
      controls.find('.pan-right').click(function () { bigPicture.pan('right', PAN_STEP); });
    }

    $viewport.mousedown(function (event) {
      if (event.button === 0) { // Left mouse button
        event.preventDefault();
        $viewport.css('cursor', 'grabbing');
        var mousePos = [event.pageY, event.pageX];
        $viewport.mousemove(function (event) {
          var diff = [mousePos[0] - event.pageY, mousePos[1] - event.pageX];
          mousePos = [event.pageY, event.pageX];
          bigPicture.pan('down', diff[0]);
          bigPicture.pan('right', diff[1]);
        });
        $(window).mouseup(function () {
          $viewport.css('cursor', 'grab');
          $viewport.unbind('mousemove');
        });
      }
    });

    $viewport.dblclick(function (event) {
      event.preventDefault();
      bigPicture.zoomIn(getImagePixelFromEvent($viewport, topLeftPx, event));
    });

    $viewport.mousewheel(function (event) {
      event.preventDefault();
      var centerPx = getImagePixelFromEvent($viewport, topLeftPx, event);
      if (event.deltaY > 0) {
        bigPicture.zoomIn(centerPx);
      } else if (event.deltaY < 0) {
        bigPicture.zoomOut(centerPx);
      }
    });
    $.extend($container, bigPicture);
  }

  $.fn.BigPicture = function (hostname) {

    return this.each(function () {
      var $container = $(this);
      $.ajax({
        url: hostname + '/image/' + $container.attr('data-big-picture') + '/meta/',
        dataType: 'json',
        context: $container,
        statusCode: {
          200: makeBigPicture,
          401: function () {
            $container.text('Hotlinking not allowed for free accounts. Upgrade ' +
              'to a premium account to use a Big-Picture on your own website.');
          },
          404: function () {
            $container.text('Image does not exist');
          }
        }
      });
    });
  };
}(jQuery));

