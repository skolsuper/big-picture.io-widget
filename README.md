# Big-Picture.io Widget #

To use our Big Picture widget on your webpage, load the css and javascript files

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: http://www.big-picture.io/static/widget/big-picture-widget.min.js
[max]: http://www.big-picture.io/static/widget/big-picture-widget.js

In your web page:

```html
<!-- Place the css tag in the <head> -->
<link href="css/big-picture.min.css" rel="stylesheet" />

...

<!-- Place the script tag at the bottom of your page so it loads after your content -->
<script src="dist/big-picture.min.js"></script>
```

Then just add a `big-picture` class to div elements in your page where you want to load a Big-Picture image. You'll need to add a `data-big-picture` attribute with the id of your image. Optionally, you can add a `no-controls` class to hide the default controls (users can pan and zoom the image with the mouse)

Example:

```
<div class="big-picture no-controls" data-big-picture="31gP1c7u4e" width="768" height="512" style="border-radius: 10px; border: 1px solid #555"></div>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
