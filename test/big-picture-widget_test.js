(function($) {
  /*
    ======== A Handy Little QUnit Reference ========
    http://api.qunitjs.com/

    Test methods:
      module(name, {[setup][ ,teardown]})
      test(name, callback)
      expect(numberOfAssertions)
      stop(increment)
      start(decrement)
    Test assertions:
      ok(value, [message])
      equal(actual, expected, [message])
      notEqual(actual, expected, [message])
      deepEqual(actual, expected, [message])
      notDeepEqual(actual, expected, [message])
      strictEqual(actual, expected, [message])
      notStrictEqual(actual, expected, [message])
      throws(block, [expected], [message])
  */

  module('jQuery#big_picture_widget', {
    // This will run before each test in this module.
    setup: function() {
      this.elems = $('#qunit-fixture').children();
    }
  });

  test('is chainable', function() {
    expect(1);
    // Not a bad test to run on collection methods.
    strictEqual(this.elems.BigPicture(), this.elems, 'should be chainable');
  });

  test('has zoom methods', function() {
    expect(2);
    strictEqual(typeof(this.elems.BigPicture().zoomIn), 'function', 'zoomIn should be a function');
    strictEqual(typeof(this.elems.BigPicture().zoomOut), 'function', 'zoomOut should be a function');
  });

  module('jQuery.big_picture_widget');

  test('is awesome', function() {
    expect(2);
    strictEqual($.big_picture_widget(), 'awesome.', 'should be awesome');
    strictEqual($.big_picture_widget({punctuation: '!'}), 'awesome!', 'should be thoroughly awesome');
  });

  module(':big_picture_widget selector', {
    // This will run before each test in this module.
    setup: function() {
      this.elems = $('#qunit-fixture').children();
    }
  });

  test('is awesome', function() {
    expect(1);
    // Use deepEqual & .get() when comparing jQuery objects.
    deepEqual(this.elems.filter(':big_picture_widget').get(), this.elems.last().get(), 'knows awesome when it sees it');
  });

}(jQuery));
